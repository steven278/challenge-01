const readline = require('readline');

const rl = readline.createInterface({
    input : process.stdin,
    output: process.stdout
});

const PI = 3.14;

const pauseProgram = () => {
    process.stdout.write("press enter to continue. . .");
    rl.on('line', () => {
        startProgram();
    });
}

const askSecondInput = (input1, secondInput, operation) => {
    rl.question(`Insert ${secondInput} : `, (input2) => {
        if(isNaN(input2) || input2 === ''){
            console.log("Sorry, please input a valid number");
            askSecondInput(input1, secondInput, operation);
        }else{
            switch(operation){
                case 'add'      : console.log(`Calculation result : ${parseInt(input1) + parseInt(input2)}`);break;
                case 'minus'    : console.log(`Calculation result : ${input1 - input2}`);break;
                case 'times'    : console.log(`Calculation result : ${input1 * input2}`);break;
                case 'divide'   : console.log(`Calculation result : ${input1 / input2}`);break;
                case 'root'     : console.log(`Calculation result : ${Math.pow(input1, 1/input2)}`);break;
                case 'pow'      : console.log(`Calculation result : ${Math.pow(input1, input2)}`);break;
                case 'tube'     : console.log(`Calculation result : ${PI * Math.pow(input1,2) * input2}`);break;
            }
            pauseProgram();
        }
    })
}

const askFirstInput = (firstInput, secondInput, operation) => {
    rl.question(`Insert ${firstInput}: `, (input1) => {
        if(isNaN(input1) || input1 === ''){
            console.log("Sorry, please input a valid number");
            askFirstInput(firstInput, secondInput, operation);
        }else if(operation === 'square'){
            console.log(`Calculation result : ${input1 * input1}`);
            pauseProgram();
        }else if(operation === 'cube'){
            console.log(`Calculation result : ${input1 * input1 * input1}`);
            pauseProgram();
        }else{
            askSecondInput(input1, secondInput, operation);
        }
        
    });
}

const chooseProgram = () =>{
    rl.question('Choice : ', (input) => {
        switch(input){
            case 'add'      : askFirstInput('first operand','second operand','add');break;
            case 'minus'    : askFirstInput('first operand','second operand','minus');break;
            case 'times'    : askFirstInput('first operand','second operand','times');break;
            case 'divide'   : askFirstInput('first operand','second operand', 'divide');break;
            case 'root'     : askFirstInput('a number','nth root', 'root');break;
            case 'pow'      : askFirstInput('a number','to the power of', 'pow');break;
            case 'sqarea'   : askFirstInput('side length','', 'square');break;
            case 'cubevol'  : askFirstInput('side length','', 'cube');break;
            case 'tubevol'  : askFirstInput('base radius','tube height','tube');break;
            case 'exit'     : console.log('Thank you for using this program'); rl.close();break;
            default         : console.log('Sorry, please input a valid choice'); chooseProgram();break;
        }
    });
}

const startProgram = () => {
    console.log(`
+----------------------------------------+
|    Welcome to the calculator Program   |
|----------------------------------------|
|1. type 'add' for addition              |
|2. type 'minus' for substraction        |
|3. type 'times' for multiplication      |
|4. type 'divide' for division           |
|5. type 'root' for nth root             |
|6. type 'pow' for power number          |
|7. type 'sqarea'  for square area       |
|8. type 'cubevol' for cube volume       |
|9. type 'tubevol' for tube volume       |
|0. type 'exit' to exit from the program |
+----------------------------------------+`);
chooseProgram();
};

startProgram();


